package se.jsib.surfacemapper.domain;

import processing.core.PImage;
import processing.opengl.PGraphicsOpenGL;

import java.awt.Polygon;

/**
 * User: ivarboson
 */
public interface Surface {

    /**
     * Scales the surface from the calculated center point.
     * scale[0] is used to scale horizontally, and scale[1] is used to scale vertically
     * @param scale
     */
    public void scale(double[] scale);

    /**
     * Re-calculates the transformation of the surface
     */
    public void updateTransform();

    /**
     * Renders the surface in calibration mode to the offscreen
     * @param offscreen
     */
    public void render(PGraphicsOpenGL offscreen);

    /**
     * Renders the surface with the texture to the offscreen
     * @param offscreen
     * @param texture
     */
    public void render(PGraphicsOpenGL offscreen, PImage texture);

    /**
     * Set the color the surface is rendered with in calibration mode
     * @param color
     */
    public void setColor(int color);

    /**
     * @return Returns the color the surface is rendered with in calibration mode
     */
    public int getColor();

    /**
     * Set the subdivision amount for the Surface.
     * The subdivision splits the surface into many small fragments and can
     * improve the the rendered output but it comes with a performance penalty.
     * @param subdivision
     */
    public void setSubdivision(int subdivision);

    /**
     * @return Returns the subdivision amount for the surface
     */
    public int getSubdivision();

    /**
     * Manually set coordinates for mapping the texture. This allows for easy
     * cropping and enables a single texture to span more than one surface.
     * @param crop
     */
    public void setTextureCrop(TextureCrop crop);

    /**
     * @return Returns the TextureCrop being used by the surface
     */
    public TextureCrop getTextureCrop();

    /**
     * Looks for a selectable point at the specified coordinates and sets it
     * to selected if a selectable point is within the selection distance of the
     * coordinates.
     * @param x
     * @param y
     */
    public void setSelectPoint(int x, int y);

    /**
     * @return Returns the id of the selected point or null if no selection has been made
     */
    public Integer getSelectPoint();

    /**
     * Set if the surface is locked, i.e. no changes can be made to it.
     * @param locked
     */
    public void setLocked(boolean locked);

    /**
     * @return Returns true if the surface is locked, false if not.
     */
    public boolean isLocked();

    /**
     * Set the name of the surface
     * @param name
     */
    public void setName(String name);

    /**
     * @return Returns the name of the surface
     */
    public String getName();

    /**
     * @return Returns the surface as a java.awt.Polygon
     */
    public Polygon getSurfaceAsPolygon();

    /**
     * Performs a check if the coordinates are inside the bounds of the surface
     * @param x
     * @param y
     * @return Returns true if the coordinates are inside the surface, false if not.
     */
    public boolean isInside(int x, int y);

    /**
     * Set if the surface should be rendered with percentage markers in calibration mode
     * This can be useful when mapping surfaces using edge blending
     * @param showMarkers
     */
    public void setShowPercentageMarkers(boolean showMarkers);

    /**
     * @return Returns true if the surface is set to show percentage markers
     */
    public boolean isShowPercentageMarkers();

    /**
     * Set an alpha mask that will be applied to the texture before rendering
     * @param surfaceMask
     */
    public void setSurfaceMask(PImage surfaceMask);

    /**
     * Set if the surface should apply the alpha mask to the texture when rendering
     * @param useSurfaceMask
     */
    public void setUsingSurfaceMask(boolean useSurfaceMask);

    /**
     * @return Returns true if the surface is set to render using the Surface mask, false if not.
     */
    public boolean isUsingSurfaceMask();

    /**
     * Set if the surface should be hidden from view
     * This will effect both calibration mode and render mode
     * @param hidden
     */
    public void setHidden(boolean hidden);

    /**
     * @return Returns true if the surface is hidden, false if it is not.
     */
    public boolean isHidden();

    /**
     * Set the mode of the surface
     * Mode 0 is render, mode 1 is calibration
     * @param mode
     */
    public void setMode(int mode);

    /**
     * @return
     */
    public int getMode();

    /**
     * Set if the surface is selected or not
     * @param selected
     */
    public void setSelected(boolean selected);

    /**
     * @return Returns true if the surface is set to selected, false if not
     */
    public boolean isSelected();

    /**
     * Set if the surface should use edge blending
     * @param usingEdgeBlend
     */
    public void setUsingEdgeBlend(boolean usingEdgeBlend);

    /**
     * @return Returns true if the surface is set to use Edge blending, false if not
     */
    public boolean isUsingEdgeBlend();

    /**
     * Sets the edge blend of the surface.
     * @param edge decides which edge is being set
     * @param size is a normalized value of the width, so 0 is 0, 1 is the total width and 0.1 is ten percent of the width
     */
    public void setEdgeBlend(int edge, double size);
}
