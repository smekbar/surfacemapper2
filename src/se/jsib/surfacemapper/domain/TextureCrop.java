package se.jsib.surfacemapper.domain;

/**
 * User: ivarboson
 */
public class TextureCrop {
    double x, y, width, height;

    public TextureCrop(){}

    public TextureCrop(double x, double y, double width, double height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public Double getX(){
        return this.x;
    }

    public Double getY(){
        return this.y;
    }

    public Double getWidth(){
        return this.width;
    }

    public Double getHeight(){
        return this.height;
    }
}
